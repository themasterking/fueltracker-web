from .models import *
from rest_framework import serializers


class UtilisateurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Utilisateur
        fields = "__all__"


class FournisseurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fournisseur
        fields = "__all__"


class StationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Station
        fields = "__all__"


class PompeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pompe
        fields = "__all__"


class TypeCarburantSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeCarburant
        fields = "__all__"


class ReservoirSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservoir
        fields = "__all__"


class IndexSerializer(serializers.ModelSerializer):
    class Meta:
        model = Index
        fields = "__all__"


class CommandeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Commande
        fields = "__all__"


class LigneCommandeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LigneCommande
        fields = "__all__"
