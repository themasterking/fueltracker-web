(function () {
    page_profile()
    widget_tabs()
})()


function page_profile() {
    const profile_form = document.querySelectorAll(".field input, .profile--change_password, .profile--buttons button")

    if (profile_form.length > 0) {
        const active_edit = document.querySelector(".profile--buttons a")
        const change_password = document.querySelector(".profile--change_password")
        const passwords = document.querySelector(".profile--passwords")
        const form = document.querySelector("form")

        const old_password = passwords.querySelector("#password")
        const new_password = passwords.querySelector("#new_password")
        const val_password = passwords.querySelector("#confirm_password")
        const save_text_change_password = change_password.innerText
        let is_password_modified = false

        profile_form.forEach(elt => elt.disabled = true)
        change_password.style.display = "none"

        active_edit.addEventListener("click", function (e) {
            e.preventDefault()
            profile_form.forEach(elt => elt.disabled = false)
            active_edit.remove()
            change_password.style.display = "block"
        })

        change_password.addEventListener("click", function (e) {
            e.preventDefault()
            const style = getComputedStyle(passwords)
            if (style.display === "block") {
                passwords.style.display = "none"
                change_password.innerText = save_text_change_password
            } else {
                passwords.style.display = "block"
                change_password.innerText = "Masquer l'espase de changement de mot de passe"
            }
            old_password.required = !old_password.required
            new_password.required = old_password.required
            val_password.required = old_password.required
            is_password_modified = !is_password_modified
        })

        form.addEventListener("submit", function (e) {
            if (is_password_modified && (!old_password.value || new_password.value !== val_password.value))
                e.preventDefault()
            else
                this.submit()
        })
    }
}